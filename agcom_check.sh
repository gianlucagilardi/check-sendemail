#!/bin/bash


cd /root

wget -e robots=off -O new.txt --wait 1 http://www.agcom.it/atti-e-provvedimenti

variable=$(diff reference.txt new.txt)

if [[ $variable -eq 0 ]]    
then    
  echo "niente da segnalare" | mailx -s "Mancata Pubblicazione su AGCOM"  g.gilardi@thefool.it

##   echo $"nothing" &> /dev/null
else    
  echo "controlla http://www.agcom.it/atti-e-provvedimenti" | mailx -s "Pubblicazione su AGCOM"  g.gilardi@thefool.it
fi

rm reference.txt
mv new.txt reference.txt


